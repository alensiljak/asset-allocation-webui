# asset-allocation-webui

The Web UI for Asset Allocation project.

The server runs on Python/Flask and the UI is a Progressive Web Application with Vue.js.

## Server

The server implementation is in Python, using Flask framework.

## Client

The client implementation is in HTML/CSS/JS, using Vue.js framework. 
Parcel will be used for compilation instead of Webpack.
