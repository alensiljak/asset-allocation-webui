# Native Client

This is a mobile client app which uses the sources from the "client" project and packages it for the mobile platforms using Apache Cordova.

Run `cordova prepare` when getting the sources for the first time as platforms are not committed to the source code repository.
