import Vue from 'vue'
import App from './App.vue'
import router from './router'

import './../node_modules/bulma/css/bulma.css';

Vue.config.productionTip = false

// new Vue({
//   el: '#app',
//   render: h => h(App)
// });

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
