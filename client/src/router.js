import Vue from 'vue'
import VueRouter from 'vue-router'

//import Current from './Current.vue';

// 1. Use plugin.
// This installs <router-view> and <router-link>,
// and injects $router and $route to all router-enabled child components
Vue.use(VueRouter)

// 2. Define route components
const Foo = { template: '<div>Welcome to foo</div>' };
const Bar = { template: '<div>Welcome to bar</div>' };
import Four from './404.vue';
import Demo from './Demo.vue';

// 2. Define some routes
// Each route should map to a component. The "component" can
// either be an actual component constructor created via
// `Vue.extend()`, or just a component options object.
const routes = [
    {
        path: '/',
        //name: 'Home',
        //meta: { layout: "no-sidebar" },
        //component: Home,
        //component: () => import("@/App")
        //component: require("@/App.vue").default
        //component: require("./App.vue"),
        //component: Four,
        // components: {
        //     header: Home,
        //     content: Foo
        // },
        component: Demo
        // props: { header: true, content: false }
    },
    { path: '/current', component: () => import('./Current.vue') },
    { path: '/page3', component: () => import('./SomeComponent.vue') },
    { path: '/foo', component: Foo },
    { path: '/bar', component: Bar },
    { path: '/four', component: Four }
];

// 3. Create the router
const router = new VueRouter({
    mode: 'history',
    // base: __dirname,
    routes: routes
});

export default router;