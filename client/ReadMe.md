# Client

## Development

Parcel is used to package the client-side code.

Invoke by running

`nxp parcel`

References:

- https://alligator.io/vuejs/vue-parceljs/

## Frameworks

- Vue.js
- Parcel, compiler
- [Bulma](https://bulma.io/), CSS framework
