"""
The server app
"""
from flask import Flask, send_from_directory, redirect

app = Flask(__name__, static_url_path='')
app.config.from_object('config')

# Register API blueprint
from api import api
app.register_blueprint(api)


def get_client_path():
    """ Construct the path to the client distribution files """
    import os
    from pathlib import Path

    client_path = app.root_path
    path = Path(client_path).resolve()
    parent = path.parent # project root
    #static_folder = app.config.get('STATIC_FOLDER')
    client_path = os.path.join(parent, "client", "dist")

    return client_path

# set the absolute path to the static folder
app.static_folder=get_client_path()


@app.route('/')
def index():
    """ Serve the client app from here """
    return app.send_static_file('index.html')


if __name__ == "__main__":
    app.run()